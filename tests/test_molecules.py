import pytest


def test_h2():
    from learnase.molecules import h2
    mol = h2(0.75)
    assert mol.get_distance(0, 1) == pytest.approx(0.75)
