import pytest


@pytest.mark.easy
def test_add():
    from learnase.add import add_numbers
    assert add_numbers(2, 2) == 4
    assert add_numbers(2, 2.1) == pytest.approx(4.1)
